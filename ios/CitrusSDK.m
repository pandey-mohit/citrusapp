//
//  CitrusSDK.m
//  CitrusApp
//
//  Created by Mohit Pandey on 16/01/18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import "CitrusSDK.h"

@implementation CitrusSDK

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(init: (RCTResponseSenderBlock)callback)
{
  NSString *str = @"You just called init method of CitrusSDK";
  callback(@[str]);
}

@end
