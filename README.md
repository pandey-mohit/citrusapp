# README #

CitrusApp is a sample application just to demonstrate a way to integrate a native functionality with react-native.

### What is this repository for? ###

CitrusApp consist of two files:

* `CitrusSDK.h` - header file, 
* `CitrusSDK.m` - implementation file, and,
* a build setting `$(SRCROOT)/../node_modules/react-native/React` in `Header Search Paths`

which will expose an `init` method to invoke a CitrusSDK lib and return a value in callback method, when invoked via JS.


### How do I get set up? ###

* Download the codebase
* run `npm install`
* run `react-native run-ios`
* App, will run and return `'You just called init method of CitrusSDK'` text in console logs.


### Need more info, Who do I talk to? ###

* Mohit Pandey | +91-9560055863